CREDIBOOK TEST

- WEB Framework: expressjs
- ORM: sequelize

how to run:
1. "npm install".
2. Change .env.copy value and rename it to .env.
3. Change config/config copy.json and rename it to config/config.json.
4. Migrate DB "npx sequelize-cli db:migrate".
5. Run API with "npm run start-dev".

how to seed:
1. Customize /seeders file.
2. Seed DB "npx sequelize-cli db:seed:all".