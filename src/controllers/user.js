const ClientError = require('../exceptions/ClientError');
const userService = require('../services/UserService');
const userValidator = require('../validator/users/index');

/**
 * GET /api/users
 * Get All Users
 */
exports.getUsers = async (req, res) => {
  const users = await userService.getUsers();
  res.status(200).json({
    status: 'success',
    data: {
      users,
    }});
};

/**
 * POST /api/users
 * Create New User
 */
exports.postUser = async (req, res) => {
  try {
    userValidator.postBody(req.body);
    const {username, password} = req.body;
    await userService.getUserByUsername(username);

    const userId = await userService.addUser(username, password);

    res.status(201).json({
      status: 'success',
      message: 'Berhasil menambahkan User',
      data: {
        userId,
      },
    });
  } catch (err) {
    if (err instanceof ClientError) {
      res.status(err.statusCode).json({
        status: 'fail',
        message: err.message,
      });
    } else {
      res.status(500).json({
        status: 'fail',
        message: 'Terjadi kegagalan pada server',
      });
    }
  }
};
