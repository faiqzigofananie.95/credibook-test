/* eslint-disable max-len */
const userService = require('../services/UserService');
const authenticationService = require('../services/AuthenticationService');
const AuthenticationError = require('../exceptions/AuthenticationError');
const authValidator = require('../validator/authentications/index');
const ClientError = require('../exceptions/ClientError');

/**
 * POST /api/login
 * Login User
 */
exports.login = async (req, res) => {
  try {
    authValidator.loginBody(req.body);
    const {username, password} = req.body;
    const id = await userService.verifyUserCredential(username, password);

    const accessToken = authenticationService.generateAccessToken(id);
    const refreshToken = authenticationService.generateRefreshToken(id);
    await authenticationService.addRefreshToken(refreshToken);

    res.status(200).json({
      status: 'success',
      message: 'Login berhasil',
      data: {
        accessToken,
        refreshToken,
      },
    });
  } catch (err) {
    if (err instanceof ClientError) {
      res.status(err.statusCode).json({
        status: 'fail',
        message: err.message,
      });
    } else {
      res.status(500).json({
        status: 'fail',
        message: 'Terjadi kegagalan pada server',
      });
    }
  }
};

/**
 * Middleware Aurhentication Check
 */
exports.hasAuth = async (req, res, next) => {
  try {
    if (!req.headers['authorization']) {
      throw new AuthenticationError('Akses user tidak ditemukan');
    }
    const payload = await authenticationService.verifyAccessToken(req.headers['authorization'].split(' ')[1]);
    req.user = {
      id: payload.payload,
    };
  } catch (err) {
    return res.status(401).json({
      status: 'fail',
      message: err.message,
    });
  }
  next();
};

/**
 * POST /api/refreshToken
 * Get New Access Token
 */
exports.refreshToken = async (req, res) => {
  try {
    authValidator.refreshTokenBody(req.body);

    const {refreshToken} = req.body;
    const {payload} = await authenticationService.verifyRefreshToken(refreshToken);
    const accessToken = authenticationService.generateAccessToken(payload);
    res.json({
      status: 'success',
      message: 'Access Token berhasil diperbarui',
      data: {
        accessToken,
      },
    });
  } catch (err) {
    if (err instanceof ClientError) {
      res.status(err.statusCode).json({
        status: 'fail',
        message: err.message,
      });
    } else {
      res.status(500).json({
        status: 'fail',
        message: 'Terjadi kegagalan pada server',
      });
    }
  }
};
