const ClientError = require('../exceptions/ClientError');
const transactionService = require('../services/TransactionService');
const transactionValidator = require('../validator/transactions/index');

/**
 * POST /api/transactions
 * Create New Transaction
 */
exports.postTransaction = async (req, res) => {
  try {
    transactionValidator.postBody(req.body);
    const {amount, notes, date, type} = req.body;

    const transactionId = await transactionService.addTransaction(
        amount,
        notes,
        date,
        type,
    );

    res.status(201).json({
      status: 'success',
      message: 'Transaksi berhasil ditambahkan',
      data: {
        transactionId,
      },
    });
  } catch (err) {
    if (err instanceof ClientError) {
      res.status(err.statusCode).json({
        status: 'fail',
        message: err.message,
      });
    } else {
      res.status(500).json({
        status: 'fail',
        message: 'Terjadi kegagalan pada server',
      });
    }
  }
};

/**
 * GET /api/transactions
 * Get All Transactions
 */
exports.getTransactions = async (req, res) => {
  const {sortBy, sortType, type, min, max, page, row} = req.query;
  const {metadata, transactions} = await transactionService.getTransactions(
      sortBy,
      sortType,
      type,
      min,
      max,
      page,
      row,
  );

  res.json({
    status: 'success',
    metadata,
    data: {
      transactions,
    },
  });
};

/**
 * GET /api/transactions/{transactionId}
 * Get Transaction By ID
 */
exports.getTransaction = async (req, res) => {
  try {
    const {transactionId} = req.params;
    const transaction = await transactionService.getTransactionById(
        transactionId,
    );

    return res.json({
      status: 'success',
      data: {
        transaction,
      },
    });
  } catch (err) {
    if (err instanceof ClientError) {
      res.status(err.statusCode).json({
        status: 'fail',
        message: err.message,
      });
    } else {
      res.status(500).json({
        status: 'fail',
        message: 'Terjadi kegagalan pada server',
      });
    }
  }
};

/**
 * PUT /api/transactions/{transactionId}
 * Update Transaction By ID
 */
exports.putTransaction = async (req, res) => {
  try {
    transactionValidator.putBody(req.body);
    const {transactionId} = req.params;
    await transactionService.editTransaction(transactionId, req.body);

    res.json({
      status: 'success',
      message: 'Transaksi berhasil diperbarui',
    });
  } catch (err) {
    if (err instanceof ClientError) {
      res.status(err.statusCode).json({
        status: 'fail',
        message: err.message,
      });
    } else {
      res.status(500).json({
        status: 'fail',
        message: 'Terjadi kegagalan pada server',
      });
    }
  }
};


/**
 * DELETE /api/transactions/{transactionId}
 * Delete Transaction By ID
 */
exports.deleteTransaction = async (req, res) => {
  try {
    const {transactionId} = req.params;
    await transactionService.deleteTransaction(transactionId);

    res.json({
      status: 'success',
      message: 'Transaksi berhasil dihapus',
    });
  } catch (err) {
    if (err instanceof ClientError) {
      res.status(err.statusCode).json({
        status: 'fail',
        message: err.message,
      });
    } else {
      res.status(500).json({
        status: 'fail',
        message: 'Terjadi kegagalan pada server',
      });
    }
  }
};


