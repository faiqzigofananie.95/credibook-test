const express = require('express');
require('dotenv').config();
const routes = require('./routes/route');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const cors = require('cors');

const app = express();
app.use(bodyParser.json({limit: '5mb', type: 'application/json'}));
app.use(bodyParser.urlencoded({limit: '5mb', extended: true}));
app.use(cors());
app.use(express.json({limit: '5mb', type: 'application/json'}));
app.use(express.urlencoded({limit: '5mb', extended: true}));
app.use(cookieParser());

/**
* Routes.
*/
routes(app);


app.listen(process.env.PORT);
