const user = require('../controllers/user');
const authentication = require('../controllers/authentication');
const transaction = require('../controllers/transaction');

const routes = (app) => {
  app.get('/', (req, res) => {
    res.send('hello world');
  });

  // Auth API
  app.route('/api/login')
      .post(authentication.login);
  app.route('/api/refreshToken')
      .post(authentication.refreshToken);

  // User API
  app.route('/api/users')
      .get(user.getUsers)
      .post(user.postUser);

  // Transaction API
  app.route('/api/transactions')
      .get(authentication.hasAuth, transaction.getTransactions)
      .post(authentication.hasAuth, transaction.postTransaction);
  app.route('/api/transactions/:transactionId')
      .get(authentication.hasAuth, transaction.getTransaction)
      .put(authentication.hasAuth, transaction.putTransaction)
      .delete(authentication.hasAuth, transaction.deleteTransaction);
};

module.exports = routes;
