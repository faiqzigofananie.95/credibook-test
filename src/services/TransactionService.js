const Transaction = require('../models/Transaction');
const {nanoid} = require('nanoid');
const InvariantError = require('../exceptions/InvariantError');
const {Op} = require('sequelize');
const NotFoundError = require('../exceptions/NotFoundError');


exports.addTransaction = async (amount, notes, date, type) =>{
  if (type !== 'income' & type !== 'expense') {
    throw new InvariantError('Tipe transaksi tidak ditemukan');
  }

  const id = `transaction-${nanoid(16)}`;
  const ISODate = new Date(date);
  const transaction = await Transaction.create({
    id,
    amount,
    notes,
    date: ISODate,
    type,
  });

  return transaction.id;
};

exports.getTransactions = async (
    sortBy = 'updatedAt',
    sortType = 'DESC',
    type = null,
    min = 0,
    max = 99999999999999,
    page = 1,
    row = 10) => {
  const skip = (page === 1) ? 0 : (Number(page) - 1) * Number(row);
  const query = {
    order: [[sortBy, sortType]],
    limit: Number(row),
    offset: skip,
  };

  let condition;
  if (type !== null) {
    condition = {
      'type': type, 'amount': {[Op.gte]: min, [Op.lte]: max}};
    query.where = condition;
  } else {
    condition = {'amount': {[Op.gte]: min, [Op.lte]: max}};
    query.where = condition;
  }

  const transactions = await Transaction.findAll(query);
  const metadata = {
    page: Number(page),
    per_page: Number(row),
    page_count: Math.ceil(transactions.length / Number(row)),
    total_record: transactions.length,
  };
  return {metadata, transactions};
};

exports.getTransactionById = async (transactionId) => {
  const transaction = await Transaction.findByPk(transactionId);
  if (!transaction) {
    throw new NotFoundError('Transaksi tidak berhasil ditemukan');
  }
  return transaction;
};

exports.editTransaction = async (transactionId,
    {amount, notes, date, type}) => {
  const ISODate = new Date(date);
  const transaction = await Transaction.update({
    amount,
    notes,
    date: ISODate,
    type,
  }, {where: {id: transactionId}});

  if (transaction[0] === 0) {
    throw new NotFoundError('Transaksi gagal diperbarui, Id tidak ditemukan');
  }
};

exports.deleteTransaction = async (transactionId) => {
  const transaction = await Transaction.destroy({where: {id: transactionId}});

  if (transaction === 0) {
    throw new NotFoundError('Transaksi gagal dihapus, Id tidak ditemukan');
  }
};
