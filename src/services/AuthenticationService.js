const Authentication = require('../models/Authentication');
const User = require('../models/User');
const InvariantError = require('../exceptions/InvariantError');
const AuthenticationError = require('../exceptions/AuthenticationError');
const jwt = require('jsonwebtoken');


exports.addRefreshToken = async (token) => {
  await Authentication.create({
    token: token,
  });
};

exports.deleteRefreshToken = async (token) => {
  await Authentication.destroy({where: {token}});
};

exports.generateAccessToken = (payload) => {
  const accessToken = jwt.sign(
      {payload},
      process.env.ACCESS_TOKEN_KEY,
      {expiresIn: '2h'},
  );

  return accessToken;
};

exports.generateRefreshToken = (payload) => {
  const refreshToken = jwt.sign(
      {payload},
      process.env.REFRESH_TOKEN_KEY,
      {expiresIn: '2h'},
  );

  return refreshToken;
};

exports.verifyAccessToken = async (accessToken) => {
  const decoded = jwt.verify(accessToken, process.env.ACCESS_TOKEN_KEY);

  const user = await User.findByPk(decoded.payload);
  if (!user) {
    throw new AuthenticationError('Invalid signature');
  }
  return decoded;
};

exports.verifyRefreshToken = async (refreshToken) => {
  const token = await Authentication.findOne({where: {token: refreshToken}});
  if (token === null) {
    throw new InvariantError('Refresh token tidak valid');
  }

  const decoded = jwt.verify(refreshToken, process.env.REFRESH_TOKEN_KEY);
  return decoded;
};


