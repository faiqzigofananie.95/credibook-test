const User = require('../models/User');
const {nanoid} = require('nanoid');
const bcrypt = require('bcrypt');
const NotFoundError = require('../exceptions/NotFoundError');
const InvariantError = require('../exceptions/InvariantError');


exports.addUser = async (username, password) => {
  const id = `user-${nanoid(16)}`;
  const hashedPassword = await bcrypt.hash(password, 10);

  const newUser = await User.create({
    id,
    username,
    password: hashedPassword,
  });

  return newUser.id;
};

exports.getUsers = async () => {
  const users = await User.findAll();

  return users;
};

exports.getUserByUsername = async (username) => {
  const user = await User.findOne({where: {username}});
  if (user) {
    throw new InvariantError('Username telah digunakan');
  }

  return user;
};

exports.verifyUserCredential = async (username, password) => {
  const user = await User.findOne({where: {username}});
  if (user === null) {
    throw new NotFoundError('Username yang anda gunakan salah');
  }

  const {id, password: hashedPassword} = user;
  const match = await bcrypt.compare(password, hashedPassword);
  if (!match) {
    throw new InvariantError('Kredensial yang Anda gunakan salah');
  }

  return id;
};
