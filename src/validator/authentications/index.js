const InvariantError = require('../../exceptions/InvariantError');
const {PostBodySchema, RefreshTokenSchema} = require('./schema');

exports.loginBody = (body) => {
  const validationResult = PostBodySchema.validate(body);
  if (validationResult.error) {
    throw new InvariantError(validationResult.error.message);
  }
};

exports.refreshTokenBody = (body) => {
  const validationResult = RefreshTokenSchema.validate(body);
  if (validationResult.error) {
    throw new InvariantError(validationResult.error.message);
  }
};
