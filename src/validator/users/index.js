const InvariantError = require('../../exceptions/InvariantError');
const {PostBodySchema} = require('./schema');

exports.postBody = (body) => {
  const validationResult = PostBodySchema.validate(body);
  if (validationResult.error) {
    throw new InvariantError(validationResult.error.message);
  }
};
