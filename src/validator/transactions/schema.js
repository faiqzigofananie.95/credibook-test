const Joi = require('joi');

const PostBodySchema = Joi.object({
  amount: Joi.number().required(),
  notes: Joi.string().required(),
  date: Joi.date().required(),
  type: Joi.string().required(),
});

const PutBodySchema = Joi.object({
  amount: Joi.number().required(),
  notes: Joi.string().required(),
  date: Joi.date().required(),
  type: Joi.string().required(),
});

module.exports = {PostBodySchema, PutBodySchema};
