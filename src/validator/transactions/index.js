const InvariantError = require('../../exceptions/InvariantError');
const {PostBodySchema, PutBodySchema} = require('./schema');

exports.postBody = (body) => {
  const validationResult = PostBodySchema.validate(body);
  if (validationResult.error) {
    throw new InvariantError(validationResult.error.message);
  }
};

exports.putBody = (body) => {
  const validationResult = PutBodySchema.validate(body);
  if (validationResult.error) {
    throw new InvariantError(validationResult.error.message);
  }
};
