const {DataTypes} = require('sequelize');
const sequelize = require('../database/postgres');

const Transaction = sequelize.define('transaction', {
  amount: {
    type: DataTypes.INTEGER,
    allowNull: false,
  },
  notes: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  date: {
    type: DataTypes.DATE,
    allowNull: false,
  },
  type: {
    type: DataTypes.STRING,
    allowNull: false,
  },
}, {paranoid: true});

module.exports = Transaction;
