const {DataTypes} = require('sequelize');
const sequelize = require('../database/postgres');

const Authentication = sequelize.define('authentication', {
  token: {
    type: DataTypes.STRING,
    allowNull: false,
  },
});

module.exports = Authentication;
